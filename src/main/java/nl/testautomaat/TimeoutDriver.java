package nl.testautomaat;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Copyright 2017 Job van den Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

public class TimeoutDriver {
    public int Duration;
    public int Interval;
    public List<Type> ExpectedExceptions;
    Calendar cal;


    public TimeoutDriver() {
        ExpectedExceptions = new ArrayList<>();
        Interval = 2;
        cal = Calendar.getInstance();
    }

    public void Execute(Runnable runCode) throws Exception {
        Calendar maxTime = Calendar.getInstance();
        Calendar Elapsed = Calendar.getInstance();

        this.SetUp();
        maxTime.add(Calendar.MILLISECOND, Duration * 1000);
        while (Elapsed.before(maxTime)) {
            Elapsed = Calendar.getInstance();
            try {
                runCode.run();
                return;
            } catch (Exception e) {
                Type theCaughtException = e.getClass();
                if (ExpectedExceptions.contains(theCaughtException) && Elapsed.before(maxTime)) {
                    System.out.printf("Expected exception thrown '%s'\n", theCaughtException);
                    Thread.sleep(Interval * 1000);
                    continue;
                } else {
                    throw e;
                }
            }
        }
    }

    private void SetUp() {
        if (this.Interval == 0 || this.Interval < 0) {
            throw new IllegalArgumentException("Interval must be set to more than zero");
        }
    }

}
