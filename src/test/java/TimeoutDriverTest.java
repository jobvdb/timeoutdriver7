import nl.testautomaat.TimeoutDriver;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by JobvandenBerg on 19-6-2017.
 */
public class TimeoutDriverTest {
    TimeoutDriver timeoutDriver;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public TimeoutDriverTest() {
        timeoutDriver = new TimeoutDriver();
        timeoutDriver.Duration = 10;
        timeoutDriver.Interval = 1;
    }

    @Test
    public void canCreateTimeoutDriver() {
        TimeoutDriver timeoutDriver = new TimeoutDriver();
        timeoutDriver = new TimeoutDriver();
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void canHandleExpectedExceptions() throws Exception {
        timeoutDriver.ExpectedExceptions.add(IndexOutOfBoundsException.class);
        timeoutDriver.Duration = 2;
        timeoutDriver.Interval = 1;
        timeoutDriver.Execute(new Test_1());
    }

    @Test
    public void stopsAfterFirstSuccess() throws Throwable {
        String expectedReturn = "Hello World";
        Calendar maxTime = Calendar.getInstance();
        maxTime.add(Calendar.SECOND, 10);
        timeoutDriver.Execute(new Test_2());

        Calendar stop = Calendar.getInstance();
        Assert.assertTrue(maxTime.after(stop));
    }

    @Test(expected = IllegalArgumentException.class)
    public void mustHaveIntervalHigherThanZero() throws Throwable {
        timeoutDriver.Interval = 0;
        timeoutDriver.Execute(new Test_3());
    }
}

class Test_1 extends Thread {
    @Override
    public void run() {
        final String returnedValue;
        List<String> list = new ArrayList<>();
        returnedValue = list.get(0);
    }
}

class Test_2 extends Thread {
    @Override
    public void run() {
        String expectedReturn = "Hello World";
        String returnedValue;
        List<String> list = new ArrayList<>();
        list.add(expectedReturn);
        returnedValue = list.get(0);
        Assert.assertSame(expectedReturn, returnedValue);
    }
}

class Test_3 extends Thread {
    @Override
    public void run() {
        //nothing
    }
}